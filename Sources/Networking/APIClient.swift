//
//  APIClient.swift
//  CS_iOS_Assignment
//
//  Created by Francisco Javier Garcia Galvan on 06/07/21.
//  Copyright © 2021 Backbase. All rights reserved.
//

import Foundation
import Combine

public protocol RequestConvertibleProtocol {

  func buildURLRequest() throws -> URLRequest
}

public typealias ServiceParameters = Encodable

public protocol ParameterEncoding {
  func encode(_ urlRequest: RequestConvertibleProtocol, with parameters: ServiceParameters?) throws -> URLRequest
}

public struct HTTPMethod: RawRepresentable, Equatable, Hashable {
  /// `CONNECT` method.
  public static let connect = HTTPMethod(rawValue: "CONNECT")
  /// `DELETE` method.
  public static let delete = HTTPMethod(rawValue: "DELETE")
  /// `GET` method.
  public static let get = HTTPMethod(rawValue: "GET")
  /// `HEAD` method.
  public static let head = HTTPMethod(rawValue: "HEAD")
  /// `OPTIONS` method.
  public static let options = HTTPMethod(rawValue: "OPTIONS")
  /// `PATCH` method.
  public static let patch = HTTPMethod(rawValue: "PATCH")
  /// `POST` method.
  public static let post = HTTPMethod(rawValue: "POST")
  /// `PUT` method.
  public static let put = HTTPMethod(rawValue: "PUT")
  /// `TRACE` method.
  public static let trace = HTTPMethod(rawValue: "TRACE")

  public let rawValue: String

  public init(rawValue: String) {
    self.rawValue = rawValue
  }
}

public protocol Routable {
  var servicePath: String { get }
  var encoder: ParameterEncoding { get }
  var parameters: ServiceParameters? { get }
  var httpMethod: HTTPMethod { get }
  var decoder: JSONDecoder { get }
}

public protocol RequestAdapter {
  func adapt(_ urlRequest: URLRequest) throws -> URLRequest
}

public protocol APIConfiguration {

  var baseURL: String { get }
  var adapter: RequestAdapter? { get }

}

extension URLRequest: RequestConvertibleProtocol {

  /// Returns a URL request or throws if an `Error` was encountered.
  public func buildURLRequest() throws -> URLRequest {
    self
  }
}

extension String {
  func asURL() throws -> URL {
    guard let url = URL(string: self) else {
      throw NSError()
    }
    return url
  }
}

final class RequestConvertible: RequestConvertibleProtocol {

  private let apiConfiguration: APIConfiguration
  private let service: Routable

  private var url: URL {
    do {
      return try apiConfiguration.baseURL.asURL()
    } catch {}
    return URL(fileURLWithPath: "")
  }

  public init(apiConfiguration: APIConfiguration, service: Routable) {
    self.apiConfiguration = apiConfiguration
    self.service = service
  }

  public func buildURLRequest() throws -> URLRequest {
    let serviceURL = service.servicePath.isEmpty ? url : url.appendingPathComponent(service.servicePath)
    var urlRequest = URLRequest(url: serviceURL,
                                cachePolicy: .reloadIgnoringLocalCacheData,
                                timeoutInterval: 60.0)
    urlRequest.httpMethod = service.httpMethod.rawValue
    let parameterEncoding = service.encoder
    urlRequest = try parameterEncoding.encode(urlRequest, with: service.parameters)

    if let adapter = apiConfiguration.adapter {
      urlRequest = try adapter.adapt(urlRequest)
    }

    return urlRequest
  }
}

public protocol APIClientProtocol {

  func perform<T: Decodable>(_ service: Routable) -> AnyPublisher<T, Error>

}

public enum APIClientError: LocalizedError {
  case cannotPerformService
  case noInternet
  case timeout
  case cancelled
}

public class APIClientDefault: APIClientProtocol {

  private let apiConfiguration: APIConfiguration
  private let session: URLSession

  // MARK: Initializer
  public init(apiConfiguration: APIConfiguration,
              sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default,
              sessionDelegate: URLSessionDelegate? = nil) {

    self.apiConfiguration = apiConfiguration
    self.session = URLSession(configuration: sessionConfiguration,
                              delegate: sessionDelegate,
                              delegateQueue: nil)
  }

  public func perform<T>(_ service: Routable) -> AnyPublisher<T, Error> where T: Decodable {

    let requestConvertible = RequestConvertible(apiConfiguration: apiConfiguration, service: service)
    guard let urlRequest = try? requestConvertible.buildURLRequest() else {
      return Fail(error: APIClientError.cannotPerformService).eraseToAnyPublisher()
    }

    let cancellable: AnyPublisher<T, Error>
    cancellable = session
      .dataTaskPublisher(for: urlRequest)
      .map(\.data)
      .decode(type: T.self, decoder: service.decoder)
      .receive(on: DispatchQueue.main)
      .eraseToAnyPublisher()

    return cancellable

  }

}

extension Encodable {
  public var dictionary: [String: Any]? {
    guard let data = try? JSONEncoder().encode(self) else { return nil }
    return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
  }
}

public struct URLEncoding: ParameterEncoding {

  public static var `default`: URLEncoding { .init() }

  public func encode(_ urlRequest: RequestConvertibleProtocol, with parameters: ServiceParameters?) throws -> URLRequest {

    var urlRequest = try urlRequest.buildURLRequest()
    guard let parameters = parameters else { return urlRequest }
    guard let url = urlRequest.url else {
      throw NSError()
    }

    if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false),
       let rawValues = parameters.dictionary, !rawValues.isEmpty {
      let percentEncodedQuery = (urlComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + query(rawValues)
      urlComponents.percentEncodedQuery = percentEncodedQuery
      urlRequest.url = urlComponents.url
    }

    return urlRequest
  }

  private func query(_ parameters: [String: Any]) -> String {
    parameters.keys.sorted(by: <).compactMap { key in
      guard let valueNotNil = parameters[key] else { return nil }
      return "\(key)=\(valueNotNil)"
    }.joined(separator: "&")

  }

}

extension Routable {

  var httpMethod: HTTPMethod { .get }

  var decoder: JSONDecoder {
    let jsonDecoder = JSONDecoder()
    jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
    return jsonDecoder
  }

}
