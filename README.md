# Networking

A package for handling the API on the example of the MovieDB's API


# Load Async image

```
AsyncImage(
url: imageURL,
placeholder: { Text("loading") },
image: { Image(uiImage: $0).resizable() }
)

```


# How to create a request

Firstly you need to define a Routable's enum

```
struct ExampleParams: Encodable { ... }

enum ExampleServices {

case fetchSomething(params: ExampleParams)

var servicePath: String { "fetchSomenthig" }
var encoder: ParameterEncoding { URLEncoding.default }
var parameters: ServiceParameters? {
switch (self) {
case .fetchSomething( let params)
return params
}
}
var httpMethod: HTTPMethod { .get }
var decoder: JSONDecoder { JSONDecoder() }


}

```

